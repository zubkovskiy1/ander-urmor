//var gulp           = require('gulp'),
//		gutil          = require('gulp-util' ),
//		sass           = require('gulp-sass'),
//		browserSync    = require('browser-sync'),
//		concat         = require('gulp-concat'),
//		uglify         = require('gulp-uglify'),
//		cleanCSS       = require('gulp-clean-css'),
//		rename         = require('gulp-rename'),
//		autoprefixer   = require('gulp-autoprefixer'),
//		notify         = require("gulp-notify");
//
//// Сервер и автообновление страницы Browsersync
//gulp.task('browser-sync', function() {
//	browserSync({
//		server: {
//			baseDir: 'app'
//		},
//		notify: false,
//		// tunnel: true,
//		// tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
//	});
//});
//
//// Минификация пользовательских скриптов проекта и JS библиотек в один файл
//gulp.task('js', function() {
//	return gulp.src([
//		'app/js/common.js', // Всегда в конце
//		])
//	//.pipe(uglify()) // Минимизировать весь js (на выбор)
//	.pipe(gulp.dest('app/js'))
//	.pipe(browserSync.reload({stream: true}));
//});
//
//gulp.task('sass', function() {
//	return gulp.src('app/sass/**/*.scss')
//	.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
//	.pipe(autoprefixer(['last 40 versions']))
//	//.pipe(cleanCSS()) // Опционально, закомментировать при отладке
//	.pipe(gulp.dest('app/css'))
//	.pipe(browserSync.reload({stream: true}));
//});
//
//gulp.task('watch', ['sass', 'js', 'browser-sync'], function() {
//	gulp.watch('app/sass/**/*.scss', ['sass']);
//	gulp.watch(['app/js/common.js'], ['js']);
//	gulp.watch('app/*.html', browserSync.reload);
//});
//
//gulp.task('default', ['watch']);




var     gulp           = require('gulp'),
		gutil          = require('gulp-util' ),
		sass           = require('gulp-sass'),
		browserSync    = require('browser-sync'),
		concat         = require('gulp-concat'),
		uglify         = require('gulp-uglify'),
		cleanCSS       = require('gulp-clean-css'),
		rename         = require('gulp-rename'),
		imagemin       = require('gulp-imagemin'),
		cache          = require('gulp-cache'),
		autoprefixer   = require('gulp-autoprefixer'),
		ftp            = require('vinyl-ftp'),
		notify         = require("gulp-notify");

// Скрипты проекта
gulp.task('scripts', function() {
	return gulp.src([
		'app/libs/jquery/dist/jquery.min.js',
		'app/libs/owl.carousel/dist/owl.carousel.js',
        'app/libs/jquery.threesixty.js',
		])
	.pipe(concat('scripts.js'))
//	.pipe(uglify())
	.pipe(gulp.dest('app/js'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('common', function() {
	return gulp.src([
		'app/js/common.js'
		])
	.pipe(gulp.dest('app/js'))
	.pipe(browserSync.reload({stream: true}));
});



gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false
	});
});

gulp.task('sass', function() {
	return gulp.src('app/sass/**/*.scss')
	.pipe(sass())
//	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
//	.pipe(cleanCSS())
	.pipe(gulp.dest('app/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', ['sass', 'scripts','common', 'browser-sync'], function() {
	gulp.watch('app/sass/**/*.scss', ['sass']);
	gulp.watch(['libs/**/*.js', 'app/js/common.js'], ['scripts']);
	gulp.watch('app/*.html', browserSync.reload);
});

gulp.task('imagemin', function() {
	return gulp.src('app/img/**/*')
	.pipe(cache(imagemin()))
	.pipe(gulp.dest('dist/img')); 
});

gulp.task('build', ['imagemin', 'sass', 'scripts', 'common'], function() {

	var buildFiles = gulp.src([
		'app/*.html',
		'app/.htaccess'
		]).pipe(gulp.dest('dist'));

	var buildCss = gulp.src([
		'app/css/main.css',
		]).pipe(gulp.dest('dist/css'));

	var buildJs = gulp.src([
		'app/js/scripts.js',
		'app/js/common.js'
		]).pipe(gulp.dest('dist/js'));

	var buildFonts = gulp.src([
		'app/fonts/**/*']
		).pipe(gulp.dest('dist/fonts'));

});

gulp.task('deploy', function() {

	var conn = ftp.create({
		host:      'hostname.com',
		user:      'username',
		password:  'userpassword',
		parallel:  10,
		log: gutil.log
	});

	var globs = [
	'dist/**',
	'dist/.htaccess',
	];
	return gulp.src(globs, {buffer: false})
	.pipe(conn.dest('/path/to/folder/on/server'));

});

gulp.task('removedist', function() { return del.sync('dist'); });
gulp.task('clearcache', function () { return cache.clearAll(); });

gulp.task('default', ['watch']);
